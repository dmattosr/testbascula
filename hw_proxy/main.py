# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import commands
import logging
import time
import subprocess
from threading import Lock


# from odoo import http
# from odoo.http import request

_logger = logging.getLogger(__name__)


# Those are the builtin raspberry pi USB modules, they should
# not appear in the list of connected devices.
BANNED_DEVICES = set([
    "0424:9514",    # Standard Microsystem Corp. Builtin Ethernet module
    "1d6b:0002",    # Linux Foundation 2.0 root hub
    "0424:ec00",    # Standard Microsystem Corp. Other Builtin Ethernet module
])


# drivers modules must add to drivers an object with a get_status() method
# so that 'status' can return the status of all active drivers
drivers = {}

# keep a list of RS-232 devices that have been recognized by a driver,
# so other drivers can skip them during probes
rs232_devices = {}  # {'/path/to/device': 'driver'}
rs232_lock = Lock() # must be held to update `rs232_devices`