
import serial
import time

ser = serial.Serial(0)

print(ser.name)

if ser.name == 'COM1':

    while True:

        x = ser.readline()
        tupla = str(x).split(" ")
        
        '''
            tupla de 3 campos
            campo1: instrucciones binarias
            campo2: peso que marcara el indicador
            campo3: final de cadena
        '''
        peso = tupla[1]
        
        speso = str(peso[:7])

        if speso.isdecimal() is True:
            pesa = int(speso)
            if pesa > 0:
                print('pesa: ' + str(pesa) + 'Kg')

        time.sleep(0.1)

ser.close()